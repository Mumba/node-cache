/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

export interface Cache {
	/**
	 * Get a value from the cache. Returns undefined if the key is not in the cache (a 'miss').
	 */
	get(key: any): any;

	/**
	 * Sets a value in the cache. Returns the value.
	 */
	set(key: any, value: any): any;

	/**
	 * Clear the cache.
	 */
	clear(): void;
}

export * from './adapters/NullCache';
export * from './adapters/MemoryCache';
