/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const safeMemoryCache = require('safe-memory-cache/map');

export interface MemoryCacheOptions {
	limit: number;
	buckets?: number;
	ttl?: number;
}

export class MemoryCache {
	private cache: any;

	constructor(memoryCacheOptions: MemoryCacheOptions) {
		this.cache = safeMemoryCache({
			limit: memoryCacheOptions.limit,
			buckets: memoryCacheOptions.buckets,
			maxTTL: memoryCacheOptions.ttl
		});
	}

	public get(key: any): any {
		return this.cache.get(key);
	}

	public set(key: any, value: any): any {
		return this.cache.set(key, value);
	}

	public clear(): void {
		return this.cache.clear();
	}
}
