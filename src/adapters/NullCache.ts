/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {Cache} from '../index';

export class NullCache implements Cache {
	public get(key: any): any {
		return void 0;
	}

	public set(key: any, value: any): any {
		return value;
	}

	public clear(): void {
		// Nothing to do.
	}
}
