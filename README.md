# Mumba Cache

Caching interfaces and implementation and Mumba microservices and web apps.

## Installation 

```sh
$ npm install --save mumba-cache
```

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm test
```

## People

The original author of _Mumba Cache_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-cache/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2017 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

