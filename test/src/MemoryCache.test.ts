/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {MemoryCache} from '../../src/index';

describe('MemoryCache unit tests.', () => {
	let instance: MemoryCache;
	const KEY = 'foo';
	const VALUE = 'bar';

	beforeEach(() => {
		instance = new MemoryCache({
			limit: 100
		});
		instance.set(KEY, VALUE);
	});

	it('should get a value from the cache', () => {
		assert.equal(instance.get(KEY), VALUE, 'should be the value');
	});

	it('should set a value', () => {
		assert.equal(instance.set('key', VALUE), VALUE, 'should be the value');
	});

	it('should clear the cache', () => {
		instance.clear();
		assert.strictEqual(instance.get(KEY), void 0, 'should have cleared the value');
	});

	it('should clear the cache if TTL is set', () => {
		instance = new MemoryCache({
			limit: 100,
			ttl: 1000
		});

		instance.set(KEY, VALUE);
		instance.clear();
		assert.strictEqual(instance.get(KEY), void 0, 'should have cleared the value');
	});
});
