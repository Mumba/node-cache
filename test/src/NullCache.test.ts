/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {NullCache} from '../../src/index';

describe('NullCache unit tests.', () => {
	let instance: NullCache;

	beforeEach(() => {
		instance = new NullCache();
	});

	it('should get a value from the cache', () => {
		assert.strictEqual(instance.get('key'), void 0, 'should be undefined');
	});

	it('should set a value', () => {
		const VALUE = 'the-value';

		assert.equal(instance.set('key', VALUE), VALUE, 'should be the value');
	});

	it('should clear the cache', () => {
		// Nothing to assert here.
		instance.clear();
	});
});
